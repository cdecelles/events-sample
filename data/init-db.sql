create database events;

use events;

CREATE TABLE `event` ( `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL, `typeId` int DEFAULT NULL, `name` varchar(255) COLLATE utf8_unicode_ci NULL, `description` varchar(255) COLLATE utf8_unicode_ci NULL, `location` varchar(255) COLLATE utf8_unicode_ci NULL, `tags` varchar(255) COLLATE utf8_unicode_ci NULL, `map` varchar(255) COLLATE utf8_unicode_ci NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into event (id, typeId, name, description, location, tags, map) values ('1', 1, 'KubeCon North America','Boston, MA','The Cloud Native Computing Foundation’s flagship conference gathers adopters and technologists from leading open source and cloud native communities', '', '!1m18!1m12!1m3!1d188820.8050372866!2d-71.11036952066837!3d42.31426470026251!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e3652d0d3d311b%3A0x787cbf240162e8a0!2sBoston%2C%20MA!5e0!3m2!1sen!2sus!4v1586460226630!5m2!1sen!2sus');

commit;
