#!/bin/bash

if [[ -z "${DB_PASSWORD}" ]]; then
  echo "The environment variable DB_PASSWORD is not set"
  exit 0;
fi

DELETE_ROW="delete from event where id = 4; commit;"
ADD_ROW="insert into event (id, typeId, name, description, location, tags, map) values ('4', 1, 'SpringOne Platform','Seattle, WA','SpringOne helps devs, cloud engineers, and executives connect all the pieces of the modern software puzzle. And your stories make it happen.', '', '!1m18!1m12!1m3!1d172139.41617259616!2d-122.48214910839704!3d47.61294318130471!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5490102c93e83355%3A0x102565466944d59a!2sSeattle%2C%20WA!5e0!3m2!1sen!2sus!4v1586459691528!5m2!1sen!2sus'); commit;"

#echo $DELETE_ROW
#echo $ADD_ROW

PS3='Please enter your choice: '
options=("initalize db" "add row" "delete row" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "initalize db")
            echo "initalize db"
            mysql -uroot -p$DB_PASSWORD < init-db.sql
	    break
            ;;
        "add row")
            echo "add row"
            mysql -uroot -p$DB_PASSWORD events -e "${ADD_ROW}"
            break
            ;;
        "delete row")
            echo "delete row"
            mysql -uroot -p$DB_PASSWORD events -e "${DELETE_ROW}"
            break
            ;;
        "Quit")
            break
            ;;
        *) echo invalid option;;
    esac
done
