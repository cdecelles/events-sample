# Events Sample

## Running the Application

The default Spring profile will use a H2 in-memory database

```./mvnw spring-boot:run```


### Using a Spring Profile that will allow the application to be backed by a MySQL database

#### Creating and populating the database

There is a helper script to create db/load data, add row, and delete row for demo purposes which is located here [dbhelper.sh](data/dbhelper.sh).  This file is usually copied to the home directory for the Terminal session used by the Service - MySQL from the cloned directory of this application.

Example

```cp /home/coder/events-sample/data/* /```

Environment variable to be set before using - DB_PASSWORD

```export DB_PASSWORD=*mysql-db-password*```

Run the db helper script
```./dbhelper```

The script can be modified as needed.

#### Configure the application to use MySQL

The [application.yml](src/main/resources/application.yml) located in src/resources directory can be modified to point to a running MySQL database for the coder profile, i.e. Service.  Add/change the values accordingly: url, username and database.

```
spring:
  profiles: coder
  jpa:
    database-platform: org.hibernate.dialect.MySQL5InnoDBDialect
    database: MYSQL
    hibernate:
      ddl-auto: none
  datasource:
    url: jdbc:mysql://localhost:3306/events
    username: root
    password:
```

#### Running the application

To run the application using the coder Spring profile.

```./mvnw spring-boot:run -Dspring-boot.run.profiles=coder```


## API Documentation

View the API doc once you start the application - http://localhost:8080/swagger-ui/index.html
