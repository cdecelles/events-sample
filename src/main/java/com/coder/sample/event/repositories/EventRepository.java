package com.coder.sample.event.repositories;

import org.springframework.data.repository.CrudRepository;

import com.coder.sample.event.domain.Event;


public interface EventRepository extends CrudRepository<Event, String> {
}
