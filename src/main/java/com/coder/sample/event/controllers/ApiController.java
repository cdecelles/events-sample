package com.coder.sample.event.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.coder.sample.event.domain.Event;
import com.coder.sample.event.repositories.EventRepository;

import io.swagger.v3.oas.annotations.Hidden;

@CrossOrigin(origins = "*")
@RestController
public class ApiController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	EventRepository repository;

	@GetMapping(value = "/events", produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<Event> events() {

		logger.info("Get all events");
		return repository.findAll();
	}

	@GetMapping("/events/{id}")
	Event getEvent(@PathVariable String id) {
		logger.info("Get event id=" + id);
		return repository.findById(id).orElseThrow(() -> new EventNotFoundException(id));
	}

	@PostMapping("/events")
	Event newEvent(@RequestBody Event newEvent) {
		logger.info("Create event=" + newEvent);
		return repository.save(newEvent);
	}

	@DeleteMapping("/events/{id}")
	void deleteEvent(@PathVariable String id) {
		logger.info("Delete event id=" + id);
		repository.deleteById(id);
	}

	private class EventNotFoundException extends RuntimeException {

		private static final long serialVersionUID = 1L;

		EventNotFoundException(String id) {
			super("Could not find event " + id);
		}
	}
}
