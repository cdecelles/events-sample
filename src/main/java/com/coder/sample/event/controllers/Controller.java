package com.coder.sample.event.controllers;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.view.RedirectView;

import com.coder.sample.event.repositories.EventRepository;

import io.swagger.v3.oas.annotations.Hidden;

@org.springframework.stereotype.Controller
public class Controller {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	EventRepository repository;

	@Hidden
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public RedirectView home(HttpServletResponse response) {
		return new RedirectView("/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config");
	}

}
