package com.coder.sample.event.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "event")
public class Event {

	@Id
	@Column(length = 40)
	private String id;
	private Integer typeID;
	private String name;
	private String location;
	private String description;
	private String tags;
	private String map;

	public Event() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Event(String id, Integer typeID, String name, String location, String description, String tags, String map) {
		super();
		this.id = id;
		this.typeID = typeID;
		this.name = name;
		this.location = location;
		this.description = description;
		this.tags = tags;
		this.map = map;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getTypeID() {
		return typeID;
	}

	public void setTypeID(Integer typeID) {
		this.typeID = typeID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		this.map = map;
	}

}
