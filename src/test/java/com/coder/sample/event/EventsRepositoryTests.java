package com.coder.sample.event;

import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.coder.sample.event.domain.Event;
import com.coder.sample.event.repositories.EventRepository;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class EventsRepositoryTests {

	@Autowired
	private EventRepository repository;

	@Test
	public void hasRows() throws Exception {
		assertTrue(repository.count() == 4);
	}

	@Test
	public void rowAdd() throws Exception {
		Event event = new Event("5", 1, "Test", "Boston, MA", "Test Event", "", "");
		repository.save(event);

		assertTrue(repository.count() == 5);
	}

	@Test
	public void rowDelete() throws Exception {
		Event event = new Event("5", 1, "Test", "Boston, MA", "Test Event", "", "");
		repository.delete(event);

		assertTrue(repository.count() == 4);
	}

	@Test
	public void rowDuplicate() throws Exception {
		Event event = new Event("5", 1, "Test", "Boston, MA", "Test Event", "", "");
		repository.save(event);

		assertTrue(repository.count() == 5);

		repository.save(event);

		assertTrue(repository.count() == 5);

		repository.delete(event);

		assertTrue(repository.count() == 4);
	}

}
